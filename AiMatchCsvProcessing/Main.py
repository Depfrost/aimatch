from tkinter.filedialog import askopenfilename


class CsvProcessor:
    def __init__(self):
        self.data = []
        self.added_header = ""

    def read_file(self):
        filename = askopenfilename()
        text_file = open(filename, "r")
        self.data = text_file.read().splitlines()
        text_file.close()

    def write_file(self, outfile):
        out_file = open(outfile, 'w+')
        self.data[0] = map(str, self.data[0])
        headerline = ",".join(self.data[0])
        print(headerline, file=out_file, end="\n")
        for line in self.data[1:]:
            if line[2] == "1":
                line = map(str, line)
                modifiedline = ",".join(line)
                print(modifiedline, file=out_file, end="\n")
        out_file.close()

    def modify_data(self):
        self.change_header()
        self.append_data()
        self.clean_data()

    def change_header(self):
        self.added_header += ",gender_p,idg_p,condtn_p,positin1_p,order_p,field_p,field_cd_p,undergra_p,mn_sat_p,tuition_p," \
                        "imprace_p,imprelig_p,from_p,zipcode_p,income_p,goal_p,date_p,go_out_p,career_p,career_c_p," \
                        "sports_p,tvsports_p,exercise_p,dining_p,museums_p,art_p,hiking_p,gaming_p,clubbing_p," \
                        "reading_p,tv_p,theater_p,movies_p,concerts_p,music_p,shopping_p,yoga_p,exphappy_p,expnum_p," \
                        "attr4_1_p,sinc4_1_p,intel4_1_p,fun4_1_p,amb4_1_p,shar4_1_p,attr2_1_p,sinc2_1_p,intel2_1_p," \
                        "fun2_1_p,amb2_1_p,shar2_1_p,attr3_1_p,sinc3_1_p,fun3_1_p,intel3_1_p,amb3_1_p,attr5_1_p," \
                        "sinc5_1_p,intel5_1_p,fun5_1_p,amb5_1_p,match_es_p,attr1_s_p,sinc1_s_p,intel1_s_p,fun1_s_p," \
                        "amb1_s_p,shar1_s_p,attr3_s_p,sinc3_s_p,intel3_s_p,fun3_s_p,amb3_s_p,satis_2_p,length_p," \
                        "numdat_2_p,attr7_2_p,sinc7_2_p,intel7_2_p,fun7_2_p,amb7_2_p,shar7_2_p,attr1_2_p,sinc1_2_p," \
                        "intel1_2_p,fun1_2_p,amb1_2_p,shar1_2_p,attr4_2_p,sinc4_2_p,intel4_2_p,fun4_2_p,amb4_2_p," \
                        "shar4_2_p,attr2_2_p,sinc2_2_p,intel2_2_p,fun2_2_p,amb2_2_p,shar2_2_p,attr3_2_p,sinc3_2_p," \
                        "intel3_2_p,fun3_2_p,amb3_2_p,attr5_2_p,sinc5_2_p,intel5_2_p,fun5_2_p,amb5_2_p,you_call_p," \
                        "them_cal_p,date_3_p,numdat_3_p,num_in_3_p,attr1_3_p,sinc1_3_p,intel1_3_p,fun1_3_p,amb1_3_p," \
                        "shar1_3_p,attr7_3_p,sinc7_3_p,intel7_3_p,fun7_3_p,amb7_3_p,shar7_3_p,attr4_3_p,sinc4_3_p," \
                        "intel4_3_p,fun4_3_p,amb4_3_p,shar4_3_p,attr2_3_p,sinc2_3_p,intel2_3_p,fun2_3_p,amb2_3_p," \
                        "shar2_3_p,attr3_3_p,sinc3_3_p,intel3_3_p,fun3_3_p,amb3_3_p,attr5_3_p,sinc5_3_p,intel5_3_p," \
                        "fun5_3_p,amb5_3_p"
        self.data[0] += self.added_header
        for i, line in enumerate(self.data):
            self.data[i] = (self.data[i]).split(',')

    def append_data(self):
        n = 0
        for line in self.data:
            n += 1
            if self.get_attrib("gender", line) == "1":
                for line2 in self.data:
                    if self.get_attrib("iid", line2) == self.get_attrib("pid", line)\
                            and self.get_attrib("pid", line2) == self.get_attrib("iid", line):
                        for i, elem in enumerate(line2):
                            name_header = (self.data[0])[i] + "_p"
                            if name_header in self.data[0]:
                                line.append(elem)
            print(n)

    def clean_data(self):
        return

    def get_attrib(self, name, line):
        index = (self.data[0]).index(name)
        return line[index]


if __name__ == '__main__':
    myCsvProcessor = CsvProcessor()
    myCsvProcessor.read_file()
    myCsvProcessor.modify_data()
    myCsvProcessor.write_file("dataModified.csv")
