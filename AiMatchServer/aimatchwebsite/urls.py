from django.urls import path

from . import views

urlpatterns = [
    path('register', views.register, name='register'),
    path('', views.index, name='index'),
    path('results', views.results, name='results'),
]