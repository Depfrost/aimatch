import urllib

import cognitive_face as CF
from io import BytesIO
from PIL import Image, ImageDraw
import requests

from pprint import pprint
from os.path import expanduser

from ai_match_project.settings import MEDIA_ROOT


def recogn(KEY, img_url):
    CF.Key.set(KEY)
    BASE_URL = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0'
    CF.BaseUrl.set(BASE_URL)
    detected = CF.face.detect(img_url)
    print(detected)

    def getRectangle(faceDictionary):
        rect = faceDictionary['faceRectangle']
        left = rect['left']
        top = rect['top']
        bottom = left + rect['height']
        right = top + rect['width']
        return ((left, top), (bottom, right))

    response = requests.get(img_url)
    img = Image.open(BytesIO(response.content))
    print(response)
    draw = ImageDraw.Draw(img)
    for face in detected:
        draw.rectangle(getRectangle(face), outline='blue')

    img.show()


def recogn2(imgpath):
    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': '76c561344e544f9faf77f13ec2a49e4b',
    }

    params = urllib.parse.urlencode({
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,blur,emotion,makeup',
    })

    url = 'https://westeurope.api.cognitive.microsoft.com/face/v1.0/detect?%s' % params

    var = r'{0}'.format(imgpath)
    var.replace('/', '\\')
    img = open(expanduser('{0}\{1}'.format(MEDIA_ROOT, var)), 'rb')
    response = requests.post(url, data=img, headers=headers)
    json_response = response.json()
    pprint(json_response)
    if response.status_code != 200:
        raise ValueError(
            'Request to Azure returned an error %s, the response is:\n%s'
            % (response.status_code, response.text)
        )
    return json_response
