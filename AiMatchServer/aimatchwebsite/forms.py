from django import forms
from django.contrib.auth.models import User

from aimatchwebsite.models import Profile, Document


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('gender', 'age', 'attr', 'sinc', 'intel', 'fun', 'amb', 'go_out', 'sports', 'exercise',
                  'dining', 'museums', 'art', 'hiking', 'gaming', 'clubbing', 'reading', 'tv', 'theater', 'movies',
                  'concerts', 'music', 'shopping', 'yoga')


class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ('document', )
