from django.apps import AppConfig


class AimatchwebsiteConfig(AppConfig):
    name = 'aimatchwebsite'
