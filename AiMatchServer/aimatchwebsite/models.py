from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver

GENDER_CHOICES = (
    (0, 'Female'),
    (1, 'Male')
)

RACE_CHOICES = (
    (1, 'Black/African American'),
    (2, 'European/Caucasian-American'),
    (3, 'Latino/Hispanic American'),
    (4, 'Asian/Pacific Islander/Asian-American'),
    (5, 'Native American'),
    (6, 'Other')
)

GO_OUT_CHOICES = (
    (1, 'Several times a week'),
    (2, 'Twice a week'),
    (3, 'Once a week'),
    (4, 'Twice a month'),
    (5, 'Once a month'),
    (6, 'Several times a year'),
    (7, 'Almost never')
)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.IntegerField(default=0, choices=GENDER_CHOICES)
    age = models.IntegerField(default=18)
    attr = models.IntegerField(default=5, verbose_name="Attractive")
    sinc = models.IntegerField(default=5, verbose_name="Sincerity")
    intel = models.IntegerField(default=5, verbose_name="Intelligence")
    fun = models.IntegerField(default=5, verbose_name="Fun")
    amb = models.IntegerField(default=5, verbose_name="Ambition")
    go_out = models.IntegerField(default=3, choices=GO_OUT_CHOICES, verbose_name="Going out")
    sports = models.IntegerField(default=5)
    exercise = models.IntegerField(default=5)
    dining = models.IntegerField(default=5)
    museums = models.IntegerField(default=5)
    art = models.IntegerField(default=5)
    hiking = models.IntegerField(default=5)
    gaming = models.IntegerField(default=5)
    clubbing = models.IntegerField(default=5)
    reading = models.IntegerField(default=5)
    tv = models.IntegerField(default=5)
    theater = models.IntegerField(default=5)
    movies = models.IntegerField(default=5)
    concerts = models.IntegerField(default=5)
    music = models.IntegerField(default=5)
    shopping = models.IntegerField(default=5)
    yoga = models.IntegerField(default=5)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()


class Document(models.Model):
    profile = models.OneToOneField(Profile, on_delete=models.CASCADE)
    description = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='documents/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    @receiver(post_save, sender=Profile)
    def create_profile_image(sender, instance, created, **kwargs):
        if created:
            Document.objects.create(profile=instance)

    @receiver(post_save, sender=Profile)
    def save_profile_image(sender, instance, **kwargs):
        instance.document.save()
