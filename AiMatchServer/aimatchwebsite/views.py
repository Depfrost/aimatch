from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.shortcuts import render, redirect

from ai_match_project.settings import MEDIA_ROOT
from aimatchwebsite.FaceAPI import recogn, recogn2
from aimatchwebsite.forms import ProfileForm, DocumentForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

from os.path import expanduser

import urllib
import json

# Create your views here.
from aimatchwebsite.models import Profile, Document


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('index')
        else:
            return redirect('register')
    else:
        form = UserCreationForm()
        context = {'form': form}
        return render(request, 'registration/register.html', context)


@login_required
@transaction.atomic
def index(request):
    image_form = DocumentForm(request.FILES, instance=request.user.profile.document)
    profile_form = ProfileForm(instance=request.user.profile)
    if request.method == 'POST':
        if 'uploadbutton' in request.POST:
            image_form = DocumentForm(request.POST, request.FILES, instance=request.user.profile.document)
            if image_form.is_valid():
                image_form.save()
                res = recogn2(request.user.profile.document.document)
                if len(res) != 0:
                    setAttributes(res, request)

        if 'submitbutton' in request.POST:
            profile_form = ProfileForm(request.POST, instance=request.user.profile)
            if profile_form.is_valid() and image_form.is_valid():
                profile_form.save()
                messages.success(request, 'Your profile was successfully updated')
                return redirect('results')
            else:
                messages.error(request, 'Please correct the error below.')
        return redirect('index')
    imgpath = request.user.profile.document.document.name
    path = ""
    if imgpath is not None:
        path = imgpath
    name = request.user.username
    return render(request, 'aimatchwebsite/index.html', {'profile_form': profile_form, 'image_form': image_form, 'path': path, 'name': name})


def setAttributes(res, request):
    age = res[0]['faceAttributes']['age']
    gender = res[0]['faceAttributes']['gender']
    request.user.profile.age = age
    if gender == 'female':
        request.user.profile.gender = 0
    else:
        request.user.profile.gender = 1
    request.user.profile.attr = getAttractiveness(res, gender)
    request.user.profile.save()


def getAttractiveness(res, gender):
    smileR = res[0]['faceAttributes']['smile']
    blurR = res[0]['faceAttributes']['blur']['value']
    happinessR = res[0]['faceAttributes']['emotion']['happiness']
    angerR = res[0]['faceAttributes']['emotion']['anger']
    contemptR = res[0]['faceAttributes']['emotion']['contempt']
    disgustR = res[0]['faceAttributes']['emotion']['disgust']
    fearR = res[0]['faceAttributes']['emotion']['fear']
    sadnessR = res[0]['faceAttributes']['emotion']['sadness']
    smileWeight = 0.6
    happinessWeight = 0.8
    angerWeight = angerR
    contemptWeight = contemptR
    disgustWeight = disgustR
    fearWeight = fearR
    sadnessWeight = sadnessR
    blurWeight = 0.2
    if gender == 'female':
        eyemakeupWeight = 0.5
        lipmakeupWeight = 0.5
        eyemakeupR = 1.0 if res[0]['faceAttributes']['makeup']['eyeMakeup'] == True else 0.0
        lipmakeupR = 1.0 if res[0]['faceAttributes']['makeup']['lipMakeup'] == True else 0.0
        return round((((smileR*0.5+0.5)*smileWeight
                + (happinessR*0.5+0.5)*happinessWeight
                + (1-blurR)*blurWeight
                + (eyemakeupR*0.5+0.5)*eyemakeupWeight
                + (lipmakeupR*0.5+0.5)*lipmakeupWeight
                + ((1-angerR)*0.5+0.5)*angerWeight
                + ((1-contemptR)*0.5+0.5)*contemptWeight
                + ((1-disgustR)*0.5+0.5)*disgustWeight
                + ((1-fearR)*0.5+0.5)*fearWeight
                + ((1-sadnessR)*0.5+0.5)*sadnessWeight)
                /(smileWeight + happinessWeight + angerWeight + contemptWeight + disgustWeight + fearWeight + sadnessWeight + blurWeight + eyemakeupWeight + lipmakeupWeight))*10)
    else:
        return round((((smileR*0.5+0.5)*smileWeight
                + (happinessR*0.5+0.5)*happinessWeight
                + (1-blurR)*blurWeight
                + ((1-angerR)*0.5+0.5)*angerWeight
                + ((1-contemptR)*0.5+0.5)*contemptWeight
                + ((1-disgustR)*0.5+0.5)*disgustWeight
                + ((1-fearR)*0.5+0.5)*fearWeight
                + ((1-sadnessR)*0.5+0.5)*sadnessWeight)
                /(smileWeight + happinessWeight + angerWeight + contemptWeight + disgustWeight + fearWeight + sadnessWeight + blurWeight))*10)


@login_required
def results(request):
    p = request.user.profile
    match_found = False
    compatibility = 0
    user = request.user
    if p.gender == 1:
        female_profiles = Profile.objects.filter(gender=0)
        for p2 in female_profiles:
            values = ["0", p2.age, p2.attr, p2.sinc, p2.intel, p2.fun, p2.amb, p.age,
                      p.go_out, p.sports, p.exercise, p.dining, p.museums, p.art, p.hiking,
                      p.gaming, p.clubbing, p.reading, p.tv, p.theater, p.movies, p.concerts, p.music,
                      p.shopping, p.yoga, p.attr, p.sinc, p.intel, p.fun, p.amb, p2.go_out, p2.sports,
                      p2.exercise, p2.dining, p2.museums, p2.art, p2.hiking, p2.gaming,
                      p2.clubbing, p2.reading, p2.tv, p2.theater, p2.movies, p2.concerts, p2.music,
                      p2.shopping, p2.yoga]
            res = call_api(values)
            if res > compatibility:
                compatibility = res
                user = p2.user
    else:
        male_profiles = Profile.objects.filter(gender=1)
        for p2 in male_profiles:
            values = ["0", p.age, p.attr, p.sinc, p.intel, p.fun, p.amb, p2.age,
                      p2.go_out, p2.sports, p2.exercise, p2.dining, p2.museums, p2.art, p2.hiking,
                      p2.gaming, p2.clubbing, p2.reading, p2.tv, p2.theater, p2.movies, p2.concerts, p2.music,
                      p2.shopping, p2.yoga, p2.attr, p2.sinc, p2.intel, p2.fun, p2.amb, p.go_out, p.sports,
                      p.exercise, p.dining, p.museums, p.art, p.hiking, p.gaming,
                      p.clubbing, p.reading, p.tv, p.theater, p.movies, p.concerts, p.music,
                      p.shopping, p.yoga]
            res = call_api(values)
            print(p2.user.username)
            print(res)
            if res > compatibility:
                compatibility = res
                user = p2.user
    imgpath = "documents/404.png"
    if compatibility >= 0.24:
        match_found = True
        imgpath = user.profile.document.document.name
        print(imgpath)
    compatibility = round(compatibility * 100)
    return render(request, 'aimatchwebsite/results.html', {'compatibility': compatibility, 'user': user, 'match': match_found, 'path': imgpath})


def call_api(values):
    data = {
        "Inputs": {
            "input1":
                {
                    """ColumnNames": ["match", "age_o", "attr_o", "sinc_o", "intel_o", "fun_o", "amb_o", "age",
                                    "go_out", "sports", "exercise", "dining", "museums", "art", "hiking",
                                    "gaming", "clubbing", "reading", "tv", "theater", "movies", "concerts", "music",
                                    "shopping", "yoga", "attr", "sinc", "intel", "fun", "amb", "go_out_p", "sports_p",
                                    "exercise_p", "dining_p", "museums_p", "art_p", "hiking_p", "gaming_p",
                                    "clubbing_p", "reading_p", "tv_p", "theater_p", "movies_p", "concerts_p", "music_p",
                                    "shopping_p", "yoga_p"],""" # Based on other perspective
                    "ColumnNames": ["match", "age_o", "attr5_1_p", "sinc5_1_p", "intel5_1_p", "fun5_1_p", "amb5_1_p",
                                    "age", "go_out", "sports", "exercise", "dining", "museums", "art",
                                    "hiking", "gaming", "clubbing", "reading", "tv", "theater", "movies", "concerts",
                                    "music", "shopping", "yoga", "attr5_1", "sinc5_1", "intel5_1", "fun5_1", "amb5_1",
                                    "go_out_p", "sports_p", "exercise_p", "dining_p", "museums_p", "art_p", "hiking_p",
                                    "gaming_p", "clubbing_p", "reading_p", "tv_p", "theater_p", "movies_p",
                                    "concerts_p", "music_p", "shopping_p", "yoga_p", ],
                    "Values": [values]
                }, },
        "GlobalParameters": {
        }
    }
    body = str.encode(json.dumps(data))

    # url = 'https://ussouthcentral.services.azureml.net/workspaces/71235f02c30b4c578482c7d90d11713e/services/ec8bcc8ae56843b792a9376be6db63b1/execute?api-version=2.0&details=true' # Based on other perspective
    url = 'https://ussouthcentral.services.azureml.net/workspaces/71235f02c30b4c578482c7d90d11713e/services/5266ce3f50a54d81bd0ddae687fcc6c8/execute?api-version=2.0&details=true' # Based on own perspective
    # api_key = 'vfJ5zna2QDRXXX2GT9ZTcd9jLenk2+/vZ+01XPJvr9/caU9MicbmWWL8ci1Lfh2AyzZ4BAfX4Yt4wvMs9RJ/pg=='  # Based on other perspective
    api_key = 'xy/u8qs5jOvfJ/Vp7AXhiNx16+3IBJScoQPgkvNVSlrurH9wjyDHaWLI03fanxQ5nyA1D2fRhGejIbEcgVBOoQ==' # Based on own perspective
    headers = {'Content-Type': 'application/json', 'Authorization': ('Bearer ' + api_key)}
    req = urllib.request.Request(url, body, headers)
    try:
        response = urllib.request.urlopen(req)
        result = response.read()
        json_data = json.loads(result.decode("utf-8"))
        return float(json_data['Results']['output1']['value']['Values'][0][48])
    except urllib.error.HTTPError as error:
        print("The request failed with status code: " + str(error.code))
        # Print the headers - they include the request ID and the timestamp, which are useful for debugging the failure
        print(error.info())
        print(json.loads(error.read()))
