# AiMatch
This project use AI to match 2 users together based on their profile.

Files:
data.csv: The original dataset from Kaggle
dataModified.csv: Our own custom dataset
data_columns.txt: List of the columns contained in our custom dataset
dataDocumentation: Contain the description of columns
AiMatchCsvProcessing: contain a python script to convert the original dataset to our custom dataset
